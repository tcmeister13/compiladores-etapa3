
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 1 "parser.y"

/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <string.h>
#include "comp_dict.h"
#include "comp_tree.h"
#include <iks_ast.h>
#define IKS_SIMBOLO_INDEFINIDO 0
#define IKS_SIMBOLO_LITERAL_INT 1
#define IKS_SIMBOLO_LITERAL_FLOAT 2
#define IKS_SIMBOLO_LITERAL_CHAR 3
#define IKS_SIMBOLO_LITERAL_STRING 4
#define IKS_SIMBOLO_LITERAL_BOOL 5
#define IKS_SIMBOLO_IDENTIFICADOR 6
#define RS_ERRO 1
#define RS_SUCESSO 0
extern int linha;
extern comp_dict_t* dicionario;
extern char* yytext;
extern char* sem_primeiro;
comp_tree_t* arvore;
int tipo_id;
int id_nodo=0;
int retorno;
int nodo_nao_valido = 0;


/* Line 189 of yacc.c  */
#line 105 "/home/aluno/compiladores-etapa3/build/parser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TK_PR_INT = 258,
     TK_PR_FLOAT = 259,
     TK_PR_BOOL = 260,
     TK_PR_CHAR = 261,
     TK_PR_STRING = 262,
     TK_PR_IF = 263,
     TK_PR_THEN = 264,
     TK_PR_ELSE = 265,
     TK_PR_WHILE = 266,
     TK_PR_DO = 267,
     TK_PR_INPUT = 268,
     TK_PR_OUTPUT = 269,
     TK_PR_RETURN = 270,
     TK_OC_LE = 271,
     TK_OC_GE = 272,
     TK_OC_EQ = 273,
     TK_OC_NE = 274,
     TK_OC_AND = 275,
     TK_OC_OR = 276,
     TK_LIT_INT = 277,
     TK_LIT_FLOAT = 278,
     TK_LIT_FALSE = 279,
     TK_LIT_TRUE = 280,
     TK_LIT_CHAR = 281,
     TK_LIT_STRING = 282,
     TK_IDENTIFICADOR = 283,
     TOKEN_ERRO = 284,
     UMINUS = 285
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 33 "parser.y"
	
	struct comp_tree_t* no;
	int ast_op;



/* Line 214 of yacc.c  */
#line 178 "/home/aluno/compiladores-etapa3/build/parser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 190 "/home/aluno/compiladores-etapa3/build/parser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  13
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   248

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  50
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  36
/* YYNRULES -- Number of rules.  */
#define YYNRULES  87
/* YYNRULES -- Number of states.  */
#define YYNSTATES  140

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   287

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    37,     2,     2,     2,     2,     2,     2,
      44,    45,    32,    30,    46,    31,     2,    33,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    40,    41,
      35,    49,    36,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    42,     2,    43,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    47,     2,    48,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    34,    38,    39
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint8 yyprhs[] =
{
       0,     0,     3,     5,     8,     9,    12,    14,    19,    22,
      23,    24,    29,    33,    40,    41,    43,    49,    53,    59,
      60,    64,    68,    72,    73,    75,    77,    79,    81,    83,
      85,    87,    89,    91,    95,    97,    99,   103,   110,   119,
     126,   133,   136,   139,   143,   145,   147,   150,   155,   156,
     158,   162,   164,   168,   172,   175,   178,   180,   182,   184,
     186,   188,   190,   192,   194,   196,   198,   200,   202,   204,
     206,   208,   210,   212,   217,   219,   221,   223,   225,   227,
     229,   231,   233,   235,   237,   239,   241,   243
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      51,     0,    -1,    52,    -1,    53,    52,    -1,    -1,    57,
      52,    -1,     1,    -1,    81,    40,    54,    41,    -1,    84,
      55,    -1,    -1,    -1,    56,    42,    83,    43,    -1,    58,
      61,    62,    -1,    81,    40,    84,    44,    59,    45,    -1,
      -1,    60,    -1,    81,    40,    84,    46,    60,    -1,    81,
      40,    84,    -1,    81,    40,    84,    41,    61,    -1,    -1,
      47,    63,    48,    -1,    64,    41,    63,    -1,    65,    41,
      63,    -1,    -1,    41,    -1,    64,    -1,    65,    -1,    67,
      -1,    68,    -1,    69,    -1,    70,    -1,    73,    -1,    74,
      -1,    47,    63,    48,    -1,    64,    -1,    65,    -1,    80,
      49,    77,    -1,     8,    44,    77,    45,     9,    66,    -1,
       8,    44,    77,    45,     9,    66,    10,    66,    -1,    11,
      44,    77,    45,    12,    66,    -1,    12,    66,    11,    44,
      77,    45,    -1,    13,    84,    -1,    14,    71,    -1,    72,
      46,    71,    -1,    72,    -1,    77,    -1,    15,    77,    -1,
      84,    44,    75,    45,    -1,    -1,    76,    -1,    77,    46,
      76,    -1,    77,    -1,    44,    77,    45,    -1,    77,    78,
      77,    -1,    31,    77,    -1,    37,    77,    -1,    79,    -1,
      30,    -1,    31,    -1,    32,    -1,    33,    -1,    21,    -1,
      20,    -1,    16,    -1,    17,    -1,    18,    -1,    19,    -1,
      35,    -1,    36,    -1,    74,    -1,    82,    -1,    80,    -1,
      84,    -1,    84,    42,    77,    43,    -1,     3,    -1,     4,
      -1,     5,    -1,     6,    -1,     7,    -1,    83,    -1,    23,
      -1,    26,    -1,    85,    -1,    25,    -1,    24,    -1,    22,
      -1,    28,    -1,    27,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   118,   118,   135,   136,   137,   150,   153,   155,   156,
     156,   156,   160,   178,   185,   185,   186,   186,   188,   188,
     190,   192,   207,   221,   222,   223,   232,   240,   241,   242,
     243,   244,   245,   246,   260,   261,   264,   279,   295,   315,
     331,   348,   361,   372,   385,   386,   388,   400,   422,   423,
     424,   437,   439,   440,   453,   463,   473,   475,   476,   477,
     478,   479,   480,   481,   482,   483,   484,   485,   486,   488,
     489,   490,   492,   494,   512,   513,   514,   515,   516,   518,
     519,   525,   531,   532,   538,   545,   557,   567
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TK_PR_INT", "TK_PR_FLOAT", "TK_PR_BOOL",
  "TK_PR_CHAR", "TK_PR_STRING", "TK_PR_IF", "TK_PR_THEN", "TK_PR_ELSE",
  "TK_PR_WHILE", "TK_PR_DO", "TK_PR_INPUT", "TK_PR_OUTPUT", "TK_PR_RETURN",
  "TK_OC_LE", "TK_OC_GE", "TK_OC_EQ", "TK_OC_NE", "TK_OC_AND", "TK_OC_OR",
  "TK_LIT_INT", "TK_LIT_FLOAT", "TK_LIT_FALSE", "TK_LIT_TRUE",
  "TK_LIT_CHAR", "TK_LIT_STRING", "TK_IDENTIFICADOR", "TOKEN_ERRO", "'+'",
  "'-'", "'*'", "'/'", "UMINUS", "'<'", "'>'", "'!'", "\"expr\"", "\"op\"",
  "':'", "';'", "'['", "']'", "'('", "')'", "','", "'{'", "'}'", "'='",
  "$accept", "s", "prog", "decl_global", "acesso_id_esp", "vetor_esp",
  "$@1", "func", "cab", "param", "lst_param", "decl_local", "corpo",
  "seq_com", "com_s", "com_b", "com_u", "atrib", "fluxo", "entrada",
  "saida", "lst_saida", "elem_saida", "retorno", "f_cham", "passag",
  "lst_p", "expr", "op", "elemento", "acesso_id", "tipo", "tipo_lit",
  "lit_int_pos", "tipo_id", "tipo_string", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
      43,    45,    42,    47,   285,    60,    62,    33,   286,   287,
      58,    59,    91,    93,    40,    41,    44,   123,   125,    61
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    50,    51,    52,    52,    52,    52,    53,    54,    55,
      56,    55,    57,    58,    59,    59,    60,    60,    61,    61,
      62,    63,    63,    63,    63,    63,    63,    64,    64,    64,
      64,    64,    64,    65,    66,    66,    67,    68,    68,    68,
      68,    69,    70,    71,    71,    72,    73,    74,    75,    75,
      76,    76,    77,    77,    77,    77,    77,    78,    78,    78,
      78,    78,    78,    78,    78,    78,    78,    78,    78,    79,
      79,    79,    80,    80,    81,    81,    81,    81,    81,    82,
      82,    82,    82,    82,    82,    83,    84,    85
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     0,     2,     1,     4,     2,     0,
       0,     4,     3,     6,     0,     1,     5,     3,     5,     0,
       3,     3,     3,     0,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     1,     1,     3,     6,     8,     6,
       6,     2,     2,     3,     1,     1,     2,     4,     0,     1,
       3,     1,     3,     3,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     6,    74,    75,    76,    77,    78,     0,     2,     0,
       0,    19,     0,     1,     3,     5,     0,     0,     0,    23,
      12,     0,    86,     0,     9,     0,     0,     0,     0,     0,
       0,    24,    23,     0,    25,    26,    27,    28,    29,    30,
      31,    32,     0,    72,     0,     7,    14,     8,     0,     0,
       0,    34,    35,     0,    41,    85,    80,    84,    83,    81,
      87,     0,     0,     0,    42,    44,    69,    45,    56,    71,
      70,    79,    82,    46,     0,    20,    23,    23,     0,     0,
      48,    19,     0,    15,     0,     0,     0,     0,     0,    54,
      55,     0,     0,    63,    64,    65,    66,    62,    61,    57,
      58,    59,    60,    67,    68,     0,    33,    21,    22,    36,
       0,     0,    49,    51,    18,    13,     0,     0,     0,     0,
       0,    52,    43,    53,    73,    47,     0,    17,    11,     0,
       0,     0,    50,     0,    37,    39,    40,    16,     0,    38
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     7,     8,     9,    23,    47,    48,    10,    11,    82,
      83,    16,    20,    33,    34,    35,    53,    36,    37,    38,
      39,    64,    65,    40,    66,   111,   112,    67,   105,    68,
      69,    12,    70,    71,    43,    72
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -98
static const yytype_int16 yypact[] =
{
      94,   -98,   -98,   -98,   -98,   -98,   -98,     7,   -98,    94,
      94,   188,   -30,   -98,   -98,   -98,   -34,   -20,    -6,    16,
     -98,    -6,   -98,    -1,   -23,   -21,    -2,    38,    -6,   190,
     190,   -98,    16,    -5,     4,    19,   -98,   -98,   -98,   -98,
     -98,   -98,    15,    -8,    24,   -98,   188,   -98,    25,   190,
     190,   -98,   -98,    58,   -98,   -98,   -98,   -98,   -98,   -98,
     -98,   190,   190,   190,   -98,    22,   -98,   212,   -98,   -98,
     -98,   -98,   -98,   212,    23,   -98,    16,    16,   190,   190,
     190,   188,    27,   -98,    39,    60,   108,   115,    36,   189,
     -98,   138,   190,   -98,   -98,   -98,   -98,   -98,   -98,   -98,
     -98,   -98,   -98,   -98,   -98,   190,   -98,   -98,   -98,   212,
     168,    41,   -98,    57,   -98,   -98,    -6,    40,    75,    79,
     190,   -98,   -98,   -98,   -98,   -98,   190,    63,   -98,    38,
      38,   145,   -98,   188,   100,   -98,   -98,   -98,    38,   -98
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -98,   -98,     6,   -98,   -98,   -98,   -98,   -98,   -98,   -98,
     -16,    37,   -98,   -29,   -25,   -22,   -97,   -98,   -98,   -98,
     -98,    29,   -98,   -98,   -18,   -98,    -7,   -24,   -98,   -98,
     -15,   -11,   -98,    45,   -10,   -98
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -11
static const yytype_int16 yytable[] =
{
      17,    41,    51,    74,    42,    52,    73,    13,    24,    41,
      18,    44,    42,    19,    41,    14,    15,    42,    54,   -10,
      21,    46,    22,    49,    25,    86,    87,    26,    27,    28,
      29,    30,   134,   135,    79,    84,    80,    89,    90,    91,
      45,   139,    50,    75,    22,    76,    25,   107,   108,    26,
      27,    28,    29,    30,   109,   110,   113,    31,    41,    41,
      77,    42,    42,    32,    78,    81,    22,    85,    92,    88,
      17,   106,   115,    93,    94,    95,    96,    97,    98,   116,
     120,   123,    55,   128,   129,    32,   125,    99,   100,   101,
     102,   130,   103,   104,    -4,     1,   131,     2,     3,     4,
       5,     6,   113,   126,    51,    51,   127,    52,    52,   133,
     138,    41,    41,    51,    42,    42,    52,   137,   114,   132,
      41,   122,    84,    42,    93,    94,    95,    96,    97,    98,
     117,    93,    94,    95,    96,    97,    98,     0,    99,   100,
     101,   102,     0,   103,   104,    99,   100,   101,   102,     0,
     103,   104,     0,   118,    93,    94,    95,    96,    97,    98,
     119,    93,    94,    95,    96,    97,    98,     0,    99,   100,
     101,   102,     0,   103,   104,    99,   100,   101,   102,     0,
     103,   104,     0,   121,    93,    94,    95,    96,    97,    98,
     136,     2,     3,     4,     5,     6,     0,     0,    99,   100,
     101,   102,     0,   103,   104,    93,    94,    95,    96,    97,
      98,   124,    55,    56,    57,    58,    59,    60,    22,     0,
       0,    61,     0,     0,   103,   104,     0,    62,    93,    94,
      95,    96,    97,    98,    63,     0,     0,     0,     0,     0,
       0,     0,    99,   100,   101,   102,     0,   103,   104
};

static const yytype_int16 yycheck[] =
{
      11,    19,    27,    32,    19,    27,    30,     0,    18,    27,
      40,    21,    27,    47,    32,     9,    10,    32,    28,    42,
      40,    44,    28,    44,     8,    49,    50,    11,    12,    13,
      14,    15,   129,   130,    42,    46,    44,    61,    62,    63,
      41,   138,    44,    48,    28,    41,     8,    76,    77,    11,
      12,    13,    14,    15,    78,    79,    80,    41,    76,    77,
      41,    76,    77,    47,    49,    41,    28,    42,    46,    11,
      81,    48,    45,    16,    17,    18,    19,    20,    21,    40,
      44,   105,    22,    43,     9,    47,    45,    30,    31,    32,
      33,    12,    35,    36,     0,     1,   120,     3,     4,     5,
       6,     7,   126,    46,   129,   130,   116,   129,   130,    46,
      10,   129,   130,   138,   129,   130,   138,   133,    81,   126,
     138,    92,   133,   138,    16,    17,    18,    19,    20,    21,
      85,    16,    17,    18,    19,    20,    21,    -1,    30,    31,
      32,    33,    -1,    35,    36,    30,    31,    32,    33,    -1,
      35,    36,    -1,    45,    16,    17,    18,    19,    20,    21,
      45,    16,    17,    18,    19,    20,    21,    -1,    30,    31,
      32,    33,    -1,    35,    36,    30,    31,    32,    33,    -1,
      35,    36,    -1,    45,    16,    17,    18,    19,    20,    21,
      45,     3,     4,     5,     6,     7,    -1,    -1,    30,    31,
      32,    33,    -1,    35,    36,    16,    17,    18,    19,    20,
      21,    43,    22,    23,    24,    25,    26,    27,    28,    -1,
      -1,    31,    -1,    -1,    35,    36,    -1,    37,    16,    17,
      18,    19,    20,    21,    44,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    30,    31,    32,    33,    -1,    35,    36
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,     3,     4,     5,     6,     7,    51,    52,    53,
      57,    58,    81,     0,    52,    52,    61,    81,    40,    47,
      62,    40,    28,    54,    84,     8,    11,    12,    13,    14,
      15,    41,    47,    63,    64,    65,    67,    68,    69,    70,
      73,    74,    80,    84,    84,    41,    44,    55,    56,    44,
      44,    64,    65,    66,    84,    22,    23,    24,    25,    26,
      27,    31,    37,    44,    71,    72,    74,    77,    79,    80,
      82,    83,    85,    77,    63,    48,    41,    41,    49,    42,
      44,    41,    59,    60,    81,    42,    77,    77,    11,    77,
      77,    77,    46,    16,    17,    18,    19,    20,    21,    30,
      31,    32,    33,    35,    36,    78,    48,    63,    63,    77,
      77,    75,    76,    77,    61,    45,    40,    83,    45,    45,
      44,    45,    71,    77,    43,    45,    46,    84,    43,     9,
      12,    77,    76,    46,    66,    66,    45,    60,    10,    66
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 118 "parser.y"
    { 
					id_nodo++;
					TipoFilhos *filhos  = NULL;
					if((yyvsp[(1) - (1)].no) !=NULL)
					{
						filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
						filhos->arv = (comp_tree_t*)(yyvsp[(1) - (1)].no);
						filhos->prox = NULL;

					}
					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_PROGRAMA,NULL,filhos,id_nodo);
					arvore = (comp_tree_t*) (yyval.no); 
					//arvore_imprime_visual(arvore); 
					return retorno; 
				;}
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 135 "parser.y"
    { (yyval.no) = (yyvsp[(2) - (2)].no); retorno = RS_SUCESSO;;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 136 "parser.y"
    { (yyval.no) = NULL; retorno = RS_SUCESSO; ;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 137 "parser.y"
    { 
						  id_nodo++;
						  TipoFilhos *filhos = NULL;
						  if((yyvsp[(2) - (2)].no)!=NULL)
						  {						  	
							filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
						  	filhos->arv = (comp_tree_t*)(yyvsp[(2) - (2)].no);
						  	filhos->prox = NULL;	
						  }
						  comp_tree_t* t1 = (comp_tree_t*)(yyvsp[(1) - (2)].no);
						  (yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
						  retorno = RS_SUCESSO;
						;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 150 "parser.y"
    { yyerrok; yyclearin; retorno = RS_ERRO; return RS_ERRO;;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 153 "parser.y"
    {(yyval.no)=NULL; ;}
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 156 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 156 "parser.y"
    {nodo_nao_valido = 1;;}
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 156 "parser.y"
    {nodo_nao_valido = 0;;}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 160 "parser.y"
    { 	
							id_nodo++;

							comp_tree_t* t_id = (comp_tree_t*)(yyvsp[(1) - (3)].no);	
							TipoFilhos *filhos = NULL;
							if((yyvsp[(3) - (3)].no)!=NULL)
							{
								filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
								filhos->arv = (comp_tree_t*)(yyvsp[(3) - (3)].no);
								filhos->prox = NULL;
								(yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t_id,filhos);	
							}
							else
								(yyval.no) = (yyvsp[(1) - (3)].no);
													
							
							;}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 179 "parser.y"
    {
				comp_tree_t* idf = (comp_tree_t*)(yyvsp[(3) - (6)].no);
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_FUNCAO,idf->entrada,NULL,idf->id);
				
				
			;}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 185 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 188 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 190 "parser.y"
    {(yyval.no) = (yyvsp[(2) - (3)].no);;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 193 "parser.y"
    {
					  id_nodo++;
					  TipoFilhos *filhos = NULL;
					  if((yyvsp[(3) - (3)].no)!=NULL)
					  {	//printf("AA\n");					  	
						filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					  	filhos->arv = (comp_tree_t*)(yyvsp[(3) - (3)].no);
					  	filhos->prox = NULL;	
					  }
					  comp_tree_t* t1 = (comp_tree_t*)(yyvsp[(1) - (3)].no);
					
					  (yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 208 "parser.y"
    {
					id_nodo++;
					TipoFilhos *filhos = NULL;
					if((yyvsp[(3) - (3)].no)!=NULL)
					{						  	
					filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*)(yyvsp[(3) - (3)].no);
					filhos->prox = NULL;	
					}
					comp_tree_t* t1 = (comp_tree_t*) (yyvsp[(1) - (3)].no);

					(yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 221 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 222 "parser.y"
    {(yyval.no) = NULL; ;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 224 "parser.y"
    { 
					id_nodo++;
				  	TipoFilhos *filhos = NULL;
				 	 comp_tree_t* t1 = (comp_tree_t*)(yyvsp[(1) - (1)].no);
					
				 	 (yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 233 "parser.y"
    {
					id_nodo++;
					TipoFilhos *filhos = NULL;
					comp_tree_t* t1 = (comp_tree_t*) (yyvsp[(1) - (1)].no);

					(yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 240 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no); ;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 241 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no); ;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 242 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 243 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 244 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 245 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 247 "parser.y"
    {
					id_nodo++;
					if((yyvsp[(2) - (3)].no)!=NULL)
					{
						TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
						filhos->arv = (comp_tree_t*) (yyvsp[(2) - (3)].no);
						filhos->prox = NULL;				
					
						(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_BLOCO,NULL,filhos,id_nodo);
					}
					else
						(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_BLOCO,NULL,NULL,id_nodo);
				;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 260 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 261 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 264 "parser.y"
    {
							id_nodo++;
							
							TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
							filhos_2->arv = (comp_tree_t*) (yyvsp[(3) - (3)].no);
							filhos_2->prox = NULL;

							TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
							filhos->arv = (comp_tree_t*) (yyvsp[(1) - (3)].no);
							filhos->prox = filhos_2;
							
														
							
							(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_ATRIBUICAO,NULL,filhos,id_nodo); ;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 280 "parser.y"
    {
					id_nodo++;

					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) (yyvsp[(6) - (6)].no);
					filhos_2->prox = NULL;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(3) - (6)].no);
					filhos->prox = filhos_2;
					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IF_ELSE ,NULL,filhos,id_nodo);
										
				;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 296 "parser.y"
    {
					id_nodo++;

					TipoFilhos *filhos_3 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_3->arv = (comp_tree_t*) (yyvsp[(8) - (8)].no);
					filhos_3->prox = NULL;
					
					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) (yyvsp[(6) - (8)].no);
					filhos_2->prox = filhos_3;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(3) - (8)].no);
					filhos->prox = filhos_2;
					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IF_ELSE ,NULL,filhos,id_nodo);					
				;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 316 "parser.y"
    {
					id_nodo++;

					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) (yyvsp[(6) - (6)].no);
					filhos_2->prox = NULL;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(3) - (6)].no);
					filhos->prox = filhos_2;
					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_WHILE_DO ,NULL,filhos,id_nodo);
										
				;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 332 "parser.y"
    {
					id_nodo++;

					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) (yyvsp[(5) - (6)].no);
					filhos_2->prox = NULL;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(2) - (6)].no);
					filhos->prox = filhos_2;
					
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_DO_WHILE ,NULL,filhos,id_nodo);
										
				;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 349 "parser.y"
    {
				id_nodo++;
				comp_tree_t* id = (comp_tree_t*) (yyvsp[(2) - (2)].no);

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);
				filhos->prox = NULL;
				
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_INPUT ,NULL,filhos,id_nodo);
									
			;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 362 "parser.y"
    { 
			  	id_nodo++;

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);
				filhos->prox = NULL;
				
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_OUTPUT ,NULL,filhos,id_nodo);
				
			;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 373 "parser.y"
    {
				id_nodo++;
				
				comp_tree_t* t1 = (comp_tree_t*) (yyvsp[(1) - (3)].no);
				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) (yyvsp[(3) - (3)].no);
				filhos->prox = NULL;				
			
				(yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				
			;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 385 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no); ;}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 386 "parser.y"
    {(yyval.no)=(yyvsp[(1) - (1)].no);;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 389 "parser.y"
    {
				id_nodo++;

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);
				filhos->prox = NULL;
				
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_RETURN ,NULL,filhos,id_nodo);
									
			;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 401 "parser.y"
    {
				id_nodo++;
				
				
				TipoFilhos *filhos_2 = NULL;
				if((yyvsp[(3) - (4)].no)!=NULL)
				{
					filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));					
					filhos_2->arv = (comp_tree_t*) (yyvsp[(3) - (4)].no);
					filhos_2->prox = NULL;
				}
				comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (4)].no);

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);

				
				filhos->prox = filhos_2;
				
				(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_CHAMADA_DE_FUNCAO ,NULL,filhos,id_nodo);
			;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 422 "parser.y"
    {(yyval.no) = NULL;;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 423 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 425 "parser.y"
    {
				id_nodo++;
				
				comp_tree_t* t1 = (comp_tree_t*) (yyvsp[(1) - (3)].no);
				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) (yyvsp[(3) - (3)].no);
				filhos->prox = NULL;				
			
				(yyval.no) = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				
			;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 437 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 439 "parser.y"
    {(yyval.no) = (yyvsp[(2) - (3)].no);;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 441 "parser.y"
    { 
					id_nodo++;					
					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) (yyvsp[(3) - (3)].no);
					filhos_2->prox = NULL;
					
					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(1) - (3)].no);
					filhos->prox = filhos_2;				

					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo((yyvsp[(2) - (3)].ast_op),NULL,filhos,id_nodo);	
				;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 454 "parser.y"
    { 
					id_nodo++;					
					
					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);
					filhos->prox = NULL;				

					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_ARIM_INVERSAO,NULL,filhos,id_nodo);	
				;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 464 "parser.y"
    { 
					id_nodo++;					
					
					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) (yyvsp[(2) - (2)].no);
					filhos->prox = NULL;				

					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_LOGICO_COMP_NEGACAO,NULL,filhos,id_nodo);	
				;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 473 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 475 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_SOMA; ;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 476 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_SUBTRACAO; ;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 477 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_MULTIPLICACAO; ;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 478 "parser.y"
    { (yyval.ast_op) =  IKS_AST_ARIM_DIVISAO; ;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 479 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_OU; ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 480 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_E; ;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 481 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_LE; ;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 482 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_GE; ;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 483 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_IGUAL; ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 484 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_DIF; ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 485 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_L; ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 486 "parser.y"
    { (yyval.ast_op) =  IKS_AST_LOGICO_COMP_G; ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 488 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 489 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 490 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 492 "parser.y"
    { 	comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (1)].no);
					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 495 "parser.y"
    { 
					id_nodo++;					
					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) (yyvsp[(3) - (4)].no);
					filhos_2->prox = NULL;	
		
					comp_tree_t* id = (comp_tree_t*) (yyvsp[(1) - (4)].no);

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);
					filhos->prox = filhos_2;				

					(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_VETOR_INDEXADO,NULL,filhos,id_nodo);	
				;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 512 "parser.y"
    { tipo_id = IKS_SIMBOLO_LITERAL_INT;	;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 513 "parser.y"
    { tipo_id = IKS_SIMBOLO_LITERAL_FLOAT;	;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 514 "parser.y"
    { tipo_id = IKS_SIMBOLO_LITERAL_BOOL;	;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 515 "parser.y"
    { tipo_id = IKS_SIMBOLO_LITERAL_CHAR;	;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 516 "parser.y"
    { tipo_id = IKS_SIMBOLO_LITERAL_STRING;	;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 518 "parser.y"
    {(yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 519 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_FLOAT,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);					  
					;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 525 "parser.y"
    { dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_SIMBOLO_LITERAL_CHAR,dicionario);
				          comp_dict_t* entrada = dicionario_LocalizarEntrada(sem_primeiro, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 531 "parser.y"
    { (yyval.no) = (yyvsp[(1) - (1)].no);;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 532 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_BOOL,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 538 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_BOOL,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 545 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_INT,dicionario);
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					  int ast_id =  IKS_AST_LITERAL;
					  if(nodo_nao_valido==0)
					  {
						id_nodo++;
				   	  	(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					}
					
					;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 557 "parser.y"
    { dicionario = dicionario_AdicionaTipo(yytext,tipo_id,dicionario);
		
				    	   comp_dict_t* entrada = dicionario_LocalizarEntrada(yytext, dicionario);
					   int ast_id =  IKS_AST_IDENTIFICADOR;
					   				   
						id_nodo++;
				   	  	(yyval.no) = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo);//(ast_id,entrada,NULL,id_nodo);
					   
					
				   	 ;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 567 "parser.y"
    {   					  
					  dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_SIMBOLO_LITERAL_STRING,dicionario);
						
					  comp_dict_t* entrada = dicionario_LocalizarEntrada(sem_primeiro, dicionario);

					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  (yyval.no) = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);

					;}
    break;



/* Line 1455 of yacc.c  */
#line 2430 "/home/aluno/compiladores-etapa3/build/parser.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 580 "parser.y"





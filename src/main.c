/*
   main.c

   Arquivo principal do analisador sintático.
*/
#include "main.h"
extern int linha;

void yyerror (char const *mensagem)
{
  fprintf (stderr, "%s na linha %d\n", mensagem, linha);
}

int main (int argc, char **argv)
{
  gv_init("saida.dot");
  int resultado = yyparse();
  gv_close();
  return resultado;
}


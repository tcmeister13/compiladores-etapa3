/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <stdlib.h>
#include "comp_dict.h"
#include "comp_tree.h"

/**
 * Cria uma nova �rvore
 * name: arvore_inicializa
 * @param
 * @return NULL
 *
 */
comp_tree_t* arvore_inicializa(){
    return NULL;
}


/**
 * Remove nodo da �rvore com referido id
 * name: arvore_remove_por_id
 * @param arv: onde ser� removido nodo
 * @param identificador: qual elemento ser� removido
 * @return Arvore atualizada
 *
 */
comp_tree_t* arvore_remove_por_id(comp_tree_t* arv, int identificador){
    TipoFilhos *aux_filhos;
    TipoFilhos *anterior;
    if(arv == NULL){return NULL;} /* arvore vazia */
    if(arv->nodo == identificador){ /* estou no nodo com o id */
        free(arv);
        printf("Nodo %d encontrado e removido!\n", identificador);
        return NULL;
    }
    /* testar cada filho, se tiver o id, remove da lista de filhos */
    aux_filhos = arv->filhos;
    anterior = aux_filhos;
    if(aux_filhos == NULL){ return arv; }
    /* se for o primeiro filho */
    if(aux_filhos->arv->nodo == identificador){
        arv->filhos = arv->filhos->prox;
        printf("Nodo %d encontrado e removido!\n", identificador);
        return arv;
    }
    while(aux_filhos != NULL){
        if(aux_filhos->arv->nodo == identificador){
            anterior->prox = aux_filhos->prox;
            free(aux_filhos);
            printf("Nodo %d encontrado e removido!\n", identificador);
            return arv;
        }
        anterior = aux_filhos;
        aux_filhos = aux_filhos->prox;
    }
    /* se n�o foi nenhum dos filhos, testar para todos os filhos (recurs�o) */
    while(aux_filhos != NULL){
        aux_filhos->arv = arvore_remove_por_id(aux_filhos->arv, identificador);
        aux_filhos = aux_filhos->prox;
    }
    return arv;
}




/**
 * Imprime na tela arvore conforme profundidade a esquerda
 * name: arvore_imprime_profundidade_a_esquerda
 * @param arv: arvore a ser impressa
 * @return
 *
 */
void arvore_imprime_profundidade_a_esquerda(comp_tree_t* arv){
    TipoFilhos *filhos;
    if(arv == NULL){
        printf("Arvore vazia\n");
        return;
    }
    filhos = arv->filhos;
    printf("%d\n", arv->nodo);
    if(arv->entrada!=NULL)
    {
    	printf("%s\n", arv->entrada->chave);
    }
    while(filhos != NULL){
        arvore_imprime_profundidade_a_esquerda(filhos->arv);
        filhos = filhos->prox;
    }
    return;
}

/**
 * Imprime arvore (printf) na tela
 * name: arvore_imprime_visual
 * @param arv: arvore a ser impressa
 * @return
 *
 */
void arvore_imprime_visual(comp_tree_t* arv){
    TipoFilhos *filhos;
    /* se nullo, imprime null */
    if(arv == NULL){
        printf("Arvore vazia\n");
        return;
    }
    filhos = arv->filhos;
    /* imprime dados do nodo atual */
    printf("Nodo id: %d\n", arv->id);
    printf("Nodo info: %d\n", arv->nodo);
	if(arv->entrada!=NULL)
	{
		printf("Entrada dicionario: %s\n", arv->entrada->chave);
	}
	else
	{
		printf("Sem entrada dicionario\n");
	}

	printf("Filhos:\n");
    /* se nodo sem filhos */
    if(arv->filhos == NULL){
        printf("\tnull\n");
        return;
    }
    /* imprime id filhos */
    while(filhos != NULL){
        printf("\t%d\n", filhos->arv->id);
        filhos = filhos->prox;
    }
    /* chama recurs�o para cada filho */
    filhos = arv->filhos;
    while(filhos != NULL){
        arvore_imprime_visual(filhos->arv);
        filhos = filhos->prox;
    }
    return;
}


/**
 * Cria nodo numa arvore e j� inclui no grafo gv
 * name: arvore_cria_nodo
 * @param id: valor padr�o de identificacao do nodo
 * @param entrada: entrada do dicionario referente ao nodo
 * @param filhos: filhos do nodo a ser criado
 * @param id_nodo: id de identificacao do nodo
 * @return Nodo �rvore com filhos
 *
 */
comp_tree_t* arvore_cria_nodo(int id, comp_dict_t* entrada, TipoFilhos* filhos, int id_nodo){

	comp_tree_t* arv = (comp_tree_t*) malloc(sizeof(comp_tree_t));
	
	
	//arv->entrada->chave = entrada->chave;
	arv->entrada = entrada;
	arv->nodo = id;
	arv->id = id_nodo;
	arv->filhos = filhos;

	if(entrada!=NULL)
	{
		gv_declare(id, arv, entrada->chave); 
	}
	else
	{
		
		gv_declare(id,arv,NULL);
	}
	
	//conecta todos os filhos ao pai rec�m criado
	TipoFilhos* aux = filhos;
	while(aux!=NULL)
	{
		gv_connect(arv,aux->arv);
		aux = aux->prox;
	}
	return arv;

}

/**
 * Cria nodo numa arvore sem incluir no grafo gv
 * name: arvore_cria_nodo_inutil
 * @param entrada: entrada do dicionario referente ao nodo
 * @param id_nodo: id de identificacao do nodo
 * @return Nodo �rvore com entrada dicionario e id
 *
 */
comp_tree_t* arvore_cria_nodo_inutil(comp_dict_t* entrada,int id_nodo){

	comp_tree_t* arv = (comp_tree_t*) malloc(sizeof(comp_tree_t));
	arv->entrada = entrada;
	arv->id = id_nodo;

	return arv;

}

/**
 * Insere filho na lista de filhos de um nodo j� criado
 * name: arvore_insere_filho
 * @param arv: nodo a ter filho incluido
 * @param novo_filho: novo filho a ser incluido
 * @return Nodo �rvore com mais um filho
 *
 */
comp_tree_t* arvore_insere_filho(comp_tree_t* arv, TipoFilhos* novo_filho){

	TipoFilhos* aux;
	if(arv->filhos!=NULL)
	{
		aux = arv->filhos;
		while(aux->prox!=NULL)
		{
			aux= aux->prox;
		}
		aux->prox = novo_filho;
		if(novo_filho!=NULL)
			gv_connect(arv,novo_filho->arv);
	}
	else
	{
		arv->filhos = novo_filho;
		if(novo_filho!=NULL)
			gv_connect(arv,novo_filho->arv);
	}
	
	return arv;
}

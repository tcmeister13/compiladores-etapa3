%{
/*
 * Clei Antonio de Souza Junior - 207298
 * Tatiana Costa Meister - 205691
 * Jayne Guerra Ceconello -  205678
*/
#include <stdio.h>
#include <string.h>
#include "comp_dict.h"
#include "comp_tree.h"
#include <iks_ast.h>
#define IKS_SIMBOLO_INDEFINIDO 0
#define IKS_SIMBOLO_LITERAL_INT 1
#define IKS_SIMBOLO_LITERAL_FLOAT 2
#define IKS_SIMBOLO_LITERAL_CHAR 3
#define IKS_SIMBOLO_LITERAL_STRING 4
#define IKS_SIMBOLO_LITERAL_BOOL 5
#define IKS_SIMBOLO_IDENTIFICADOR 6
#define RS_ERRO 1
#define RS_SUCESSO 0
extern int linha;
extern comp_dict_t* dicionario;
extern char* yytext;
char *sem_ultimo;
char *sem_primeiro;
comp_tree_t* arvore;
int tipo_id;
int id_nodo=0;
int retorno;
int nodo_nao_valido = 0;
%}

%union
{	
	struct comp_tree_t* no;
	int ast_op;
}
/* Declaração dos tokens da gramática da Linguagem K */
%token<no> TK_PR_INT
%token<no> TK_PR_FLOAT
%token<no> TK_PR_BOOL
%token<no> TK_PR_CHAR
%token<no> TK_PR_STRING
%token<no> TK_PR_IF
%token<no> TK_PR_THEN
%token<no> TK_PR_ELSE
%token<no> TK_PR_WHILE
%token<no> TK_PR_DO
%token<no> TK_PR_INPUT
%token<no> TK_PR_OUTPUT
%token<no> TK_PR_RETURN
%token<no> TK_OC_LE
%token<no> TK_OC_GE
%token<no> TK_OC_EQ
%token<no> TK_OC_NE
%token<no> TK_OC_AND
%token<no> TK_OC_OR
%token<no> TK_LIT_INT
%token<no> TK_LIT_FLOAT
%token<no> TK_LIT_FALSE
%token<no> TK_LIT_TRUE
%token<no> TK_LIT_CHAR
%token<no> TK_LIT_STRING
%token<no> TK_IDENTIFICADOR
%token<no> TOKEN_ERRO


%type<no> func
%type<no> com_s
%type<no> atrib
%type<no> corpo
%type<no> seq_com
%type<no> prog
%type<no> s

%type<no> decl_global
%type<no> acesso_id_esp
%type<no> vetor_esp
%type<no> cab
%type<no> param
%type<no> lst_param
%type<no> decl_local
%type<no> com_b
%type<no> com_u
%type<no> fluxo
%type<no> entrada
%type<no> saida
%type<no> lst_saida
%type<no> elem_saida
%type<no> retorno
%type<no> f_cham
%type<no> passag
%type<no> lst_p
%type<no> expr
%type<ast_op> op
%type<no> elemento
%type<no> acesso_id
%type<no> tipo
%type<no> tipo_lit
%type<no> lit_int_pos
%type<no> tipo_id
%type<no> tipo_string

%start s


%left '+' '-' 
%left '*' '/'
%left UMINUS
%left TK_OC_OR TK_OC_AND TK_OC_LE TK_OC_GE TK_OC_EQ TK_OC_NE '<' '>' '!'
%left TK_LIT_STRING
%right TK_PR_ELSE TK_PR_THEN
%right "expr"
%right "op"

%%
 /* Regras (e ações) da gramática da Linguagem K */

s		:	prog	{ 
					id_nodo++;
					TipoFilhos *filhos  = NULL;
					if($1 !=NULL)
					{
						filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
						filhos->arv = (comp_tree_t*)$1;
						filhos->prox = NULL;

					}
					
					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_PROGRAMA,NULL,filhos,id_nodo);
					arvore = (comp_tree_t*) $$; 
					//arvore_imprime_visual(arvore); 
					return retorno; 
				};
			
prog		:	  decl_global prog 	{ $$ = $2; retorno = RS_SUCESSO;}
			| /* empty */ 		{ $$ = NULL; retorno = RS_SUCESSO; }
			| func prog		{ 
						  id_nodo++;
						  TipoFilhos *filhos = NULL;
						  if($2!=NULL)
						  {						  	
							filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
						  	filhos->arv = (comp_tree_t*)$2;
						  	filhos->prox = NULL;	
						  }
						  comp_tree_t* t1 = (comp_tree_t*)$1;
						  $$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
						  retorno = RS_SUCESSO;
						}
			| error 		{ yyerrok; yyclearin; retorno = RS_ERRO; return RS_ERRO;};
			

decl_global	:	tipo ':' acesso_id_esp ';' {$$=NULL; };

acesso_id_esp	:	tipo_id vetor_esp;
vetor_esp	:	/* empty */ {$$ = NULL;} | {nodo_nao_valido = 1;}'[' lit_int_pos ']'{nodo_nao_valido = 0;};



func		:	cab decl_local corpo 		{ 	
							id_nodo++;

							comp_tree_t* t_id = (comp_tree_t*)$1;	
							TipoFilhos *filhos = NULL;
							if($3!=NULL)
							{
								filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
								filhos->arv = (comp_tree_t*)$3;
								filhos->prox = NULL;
								$$ = (struct comp_tree_t*) arvore_insere_filho(t_id,filhos);	
							}
							else
								$$ = $1;
													
							
							} ;

cab		:	tipo ':' tipo_id  '(' param ')' 
			{
				comp_tree_t* idf = (comp_tree_t*)$3;
				$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_FUNCAO,idf->entrada,NULL,idf->id);
				
				
			};
param		:	/* empty */ {$$ = NULL;} | lst_param ;
lst_param	:	tipo ':' tipo_id ',' lst_param | tipo ':' tipo_id;

decl_local	:	 tipo ':' tipo_id ';' decl_local  | /* empty */ {$$ = NULL;};

corpo		:	'{' seq_com '}' {$$ = $2;} ;

seq_com		:	com_s ';' seq_com 
				{
					  id_nodo++;
					  TipoFilhos *filhos = NULL;
					  if($3!=NULL)
					  {	//printf("AA\n");					  	
						filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					  	filhos->arv = (comp_tree_t*)$3;
					  	filhos->prox = NULL;	
					  }
					  comp_tree_t* t1 = (comp_tree_t*)$1;
					
					  $$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				} 
			| com_b ';' seq_com  
				{
					id_nodo++;
					TipoFilhos *filhos = NULL;
					if($3!=NULL)
					{						  	
					filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*)$3;
					filhos->prox = NULL;	
					}
					comp_tree_t* t1 = (comp_tree_t*) $1;

					$$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				}
			| /* empty */ {$$ = NULL;} 
			| ';' {$$ = NULL; } 
			| com_s 
				{ 
					id_nodo++;
				  	TipoFilhos *filhos = NULL;
				 	 comp_tree_t* t1 = (comp_tree_t*)$1;
					
				 	 $$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				} 
			| com_b  
				{
					id_nodo++;
					TipoFilhos *filhos = NULL;
					comp_tree_t* t1 = (comp_tree_t*) $1;

					$$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				};
com_s		: 	  atrib {$$ = $1; } 
			| fluxo {$$ = $1; } 
			| entrada {$$ = $1;} 
			| saida {$$ = $1;} 
			| retorno {$$ = $1;} 
			| f_cham {$$ = $1;} ;
com_b		:	'{' seq_com '}' 
				{
					id_nodo++;
					if($2!=NULL)
					{
						TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
						filhos->arv = (comp_tree_t*) $2;
						filhos->prox = NULL;				
					
						$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_BLOCO,NULL,filhos,id_nodo);
					}
					else
						$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_BLOCO,NULL,NULL,id_nodo);
				};
com_u		: 	  com_s {$$ = $1;} 
			| com_b {$$ = $1;} ;


atrib		:	acesso_id '=' expr 	{
							id_nodo++;
							
							TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
							filhos_2->arv = (comp_tree_t*) $3;
							filhos_2->prox = NULL;

							TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
							filhos->arv = (comp_tree_t*) $1;
							filhos->prox = filhos_2;
							
														
							
							$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_ATRIBUICAO,NULL,filhos,id_nodo); }  ;

fluxo		: 	  TK_PR_IF '(' expr ')' TK_PR_THEN com_u 
				{
					id_nodo++;

					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) $6;
					filhos_2->prox = NULL;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $3;
					filhos->prox = filhos_2;
					
					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IF_ELSE ,NULL,filhos,id_nodo);
										
				}
			| TK_PR_IF '(' expr ')' TK_PR_THEN com_u TK_PR_ELSE com_u 
				{
					id_nodo++;

					TipoFilhos *filhos_3 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_3->arv = (comp_tree_t*) $8;
					filhos_3->prox = NULL;
					
					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) $6;
					filhos_2->prox = filhos_3;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $3;
					filhos->prox = filhos_2;
					
					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IF_ELSE ,NULL,filhos,id_nodo);					
				}
	
			| TK_PR_WHILE '(' expr ')' TK_PR_DO com_u
				{
					id_nodo++;

					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) $6;
					filhos_2->prox = NULL;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $3;
					filhos->prox = filhos_2;
					
					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_WHILE_DO ,NULL,filhos,id_nodo);
										
				}
			| TK_PR_DO com_u TK_PR_WHILE '(' expr ')' 
				{
					id_nodo++;

					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) $5;
					filhos_2->prox = NULL;
					

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $2;
					filhos->prox = filhos_2;
					
					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_DO_WHILE ,NULL,filhos,id_nodo);
										
				}; 

entrada		:	TK_PR_INPUT tipo_id
			{
				id_nodo++;
				comp_tree_t* id = (comp_tree_t*) $2;

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);
				filhos->prox = NULL;
				
				$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_INPUT ,NULL,filhos,id_nodo);
									
			};

saida		:	TK_PR_OUTPUT lst_saida
			{ 
			  	id_nodo++;

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) $2;
				filhos->prox = NULL;
				
				$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_OUTPUT ,NULL,filhos,id_nodo);
				
			};
lst_saida	:	elem_saida ',' lst_saida 
			{
				id_nodo++;
				
				comp_tree_t* t1 = (comp_tree_t*) $1;
				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) $3;
				filhos->prox = NULL;				
			
				$$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				
			}
			| elem_saida {$$ = $1; } ;
elem_saida	:	expr {$$=$1;} ;

retorno		:	TK_PR_RETURN expr
			{
				id_nodo++;

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) $2;
				filhos->prox = NULL;
				
				$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_RETURN ,NULL,filhos,id_nodo);
									
			}; ;

f_cham		:	tipo_id '(' passag ')' 
			{
				id_nodo++;
				
				
				TipoFilhos *filhos_2 = NULL;
				if($3!=NULL)
				{
					filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));					
					filhos_2->arv = (comp_tree_t*) $3;
					filhos_2->prox = NULL;
				}
				comp_tree_t* id = (comp_tree_t*) $1;

				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);

				
				filhos->prox = filhos_2;
				
				$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_CHAMADA_DE_FUNCAO ,NULL,filhos,id_nodo);
			};
passag		:	/* empty */ {$$ = NULL;}
			| lst_p {$$ = $1;} ;
lst_p		:	expr ',' lst_p 
			{
				id_nodo++;
				
				comp_tree_t* t1 = (comp_tree_t*) $1;
				TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
				filhos->arv = (comp_tree_t*) $3;
				filhos->prox = NULL;				
			
				$$ = (struct comp_tree_t*) arvore_insere_filho(t1,filhos);
				
				
			}
			| expr {$$ = $1;};

expr		:	  '(' expr ')' {$$ = $2;} 
			| expr op %prec "expr" expr 
				{ 
					id_nodo++;					
					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) $3;
					filhos_2->prox = NULL;
					
					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $1;
					filhos->prox = filhos_2;				

					$$ = (struct comp_tree_t*) arvore_cria_nodo($2,NULL,filhos,id_nodo);	
				}
			| '-' expr  %prec UMINUS
				{ 
					id_nodo++;					
					
					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $2;
					filhos->prox = NULL;				

					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_ARIM_INVERSAO,NULL,filhos,id_nodo);	
				}
			| '!' expr
				{ 
					id_nodo++;					
					
					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = (comp_tree_t*) $2;
					filhos->prox = NULL;				

					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_LOGICO_COMP_NEGACAO,NULL,filhos,id_nodo);	
				}	
			| elemento {$$ = $1;};

op		: 	  '+'  { $$ =  IKS_AST_ARIM_SOMA; }
			| '-'  { $$ =  IKS_AST_ARIM_SUBTRACAO; }	 	
			| '*'  { $$ =  IKS_AST_ARIM_MULTIPLICACAO; }	
			| '/'  { $$ =  IKS_AST_ARIM_DIVISAO; }		
			| TK_OC_OR { $$ =  IKS_AST_LOGICO_OU; }
			| TK_OC_AND %prec "op" { $$ =  IKS_AST_LOGICO_E; }
			| TK_OC_LE { $$ =  IKS_AST_LOGICO_COMP_LE; }
			| TK_OC_GE { $$ =  IKS_AST_LOGICO_COMP_GE; }
			| TK_OC_EQ { $$ =  IKS_AST_LOGICO_COMP_IGUAL; }
			| TK_OC_NE { $$ =  IKS_AST_LOGICO_COMP_DIF; }
			| '<' { $$ =  IKS_AST_LOGICO_COMP_L; }
			| '>' { $$ =  IKS_AST_LOGICO_COMP_G; };

elemento	:	f_cham {$$ = $1;}
			| tipo_lit {$$ = $1;}
			| acesso_id {$$ = $1;};

acesso_id	:	  tipo_id { 	comp_tree_t* id = (comp_tree_t*) $1;
					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);} 
			| tipo_id '[' expr ']' 
				{ 
					id_nodo++;					
					TipoFilhos *filhos_2 = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos_2->arv = (comp_tree_t*) $3;
					filhos_2->prox = NULL;	
		
					comp_tree_t* id = (comp_tree_t*) $1;

					TipoFilhos *filhos = (TipoFilhos*) malloc(sizeof(TipoFilhos));
					filhos->arv = arvore_cria_nodo(IKS_AST_IDENTIFICADOR,id->entrada,NULL,id->id);
					filhos->prox = filhos_2;				

					$$ = (struct comp_tree_t*) arvore_cria_nodo(IKS_AST_VETOR_INDEXADO,NULL,filhos,id_nodo);	
				};



tipo		:	  TK_PR_INT 	{ tipo_id = IKS_SIMBOLO_LITERAL_INT;	}
			| TK_PR_FLOAT 	{ tipo_id = IKS_SIMBOLO_LITERAL_FLOAT;	} 
			| TK_PR_BOOL 	{ tipo_id = IKS_SIMBOLO_LITERAL_BOOL;	} 
			| TK_PR_CHAR  	{ tipo_id = IKS_SIMBOLO_LITERAL_CHAR;	} 
			| TK_PR_STRING 	{ tipo_id = IKS_SIMBOLO_LITERAL_STRING;	};

tipo_lit	: 	  lit_int_pos {$$ = $1;}
			| TK_LIT_FLOAT 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_FLOAT,dicionario);
					  comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);					  
					} 
			| TK_LIT_CHAR 	{ sem_ultimo = (char*)malloc(strlen(yytext)-1); 
					  strncpy(sem_ultimo,yytext,strlen(yytext)-1); 
					  sem_primeiro = &sem_ultimo[1]; 
					  dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_SIMBOLO_LITERAL_CHAR,dicionario);
				          comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(sem_primeiro, dicionario));
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					} 
			| tipo_string {$$ = $1;}
			| TK_LIT_TRUE 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_BOOL,dicionario);
					  comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					} 
			| TK_LIT_FALSE 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_BOOL,dicionario);
					  comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					};

lit_int_pos	:	  TK_LIT_INT 	{ dicionario = dicionario_AdicionaTipo(yytext,IKS_SIMBOLO_LITERAL_INT,dicionario);
					  comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
					  int ast_id =  IKS_AST_LITERAL;
					  if(nodo_nao_valido==0)
					  {
						id_nodo++;
				   	  	$$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					}
					
					};


tipo_id		:	TK_IDENTIFICADOR { dicionario = dicionario_AdicionaTipo(yytext,tipo_id,dicionario);
				    	   comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(yytext, dicionario));
					   int ast_id =  IKS_AST_IDENTIFICADOR;
					   
					   
						id_nodo++;
				   	  	$$ = (struct comp_tree_t*) arvore_cria_nodo_inutil(entrada,id_nodo);//(ast_id,entrada,NULL,id_nodo);
					   
					
				   	 } ;
tipo_string	:	TK_LIT_STRING {   sem_ultimo = (char*)malloc(strlen(yytext)-1); 
					  strncpy(sem_ultimo,yytext,strlen(yytext)-1); 
					  sem_primeiro = &sem_ultimo[1]; 
					  dicionario = dicionario_AdicionaTipo(sem_primeiro,IKS_SIMBOLO_LITERAL_STRING,dicionario);
					  comp_dict_t* entrada = dicionario_Criar_Entrada(dicionario_LocalizarEntrada(sem_primeiro, dicionario));
					  int ast_id =  IKS_AST_LITERAL;
					  id_nodo++;
					  $$ = (struct comp_tree_t*) arvore_cria_nodo(ast_id,entrada,NULL,id_nodo);
					} ;
					 


%%


